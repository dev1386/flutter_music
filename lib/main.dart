import 'package:flutter/material.dart';
import 'package:music/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Deval Music',
      home: new Home(),
      debugShowCheckedModeBanner: false,
    );
  }

}
