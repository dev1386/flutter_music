import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:music/music.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _Home();
  }

}

class _Home extends State<Home> {

  List<Music> musics = [
    new Music('Theme swift', 'Codabee', 'images/un.jpg', 'https://codabee.com/wp-content/uploads/2018/06/un.mp3'),
    new Music('Theme Flutter', 'Codabee', 'images/deux.jpg', 'https://codabee.com/wp-content/uploads/2018/06/deux.mp3')
  ];

  Music currentMusic;
  Duration position = new Duration(seconds: 0);
  AudioPlayer audioPlayer;
  StreamSubscription positionSubscription;
  StreamSubscription stateSubscription;
  Duration duration = new Duration(seconds: 0);
  PlayerState status = PlayerState.stopped;
  int index = 0;

  @override
  void initState() {
    super.initState();
    currentMusic = musics[index];
    configureAudioPlayer();
  }

  Text textWithStyle(String data, double scale) {
    return new Text(
      data,
      textScaleFactor: scale,
      textAlign: TextAlign.center,
      style: new TextStyle(
        color: Colors.white,
        fontSize: 20.0,
        fontStyle: FontStyle.italic 
      ),
    );
  }

  IconButton button(IconData iconData, double size, MusicAction musicAction) {
    return new IconButton(
        icon: new Icon(
          iconData,
          color: Colors.white,
        ),
        iconSize: size,
        onPressed: () {
          action(musicAction);
        },
    );
  }

  void action(MusicAction musicAction) {
    switch(musicAction) {
      case MusicAction.play:
        play();
        break;
      case MusicAction.pause:
        pause();
        break;
      case MusicAction.rewind:
        rewind();
        break;
      case MusicAction.forward:
        forward();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Deval Music'
        ),
        backgroundColor: Colors.grey[900],
        centerTitle: true,
      ),
      backgroundColor: Colors.grey[800],
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Card(
              elevation: 9.0,
              child: new Container(
                width: MediaQuery.of(context).size.height / 2.5,
                child: new Image.asset(
                  currentMusic.imagePath
                ),
              ),
            ),
            textWithStyle(currentMusic.title, 1.5),
            textWithStyle(currentMusic.artiste, 1.0),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                button(Icons.fast_rewind, 30.0, MusicAction.rewind),
                button((this.status == PlayerState.playing) ? Icons.pause : Icons.play_arrow, 45.0, (this.status == PlayerState.playing) ? MusicAction.pause : MusicAction.play),
                button(Icons.fast_forward, 30.0, MusicAction.forward),
              ],
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                textWithStyle(getDuration(position), 0.8),
                textWithStyle(getDuration(duration), 0.8),
              ],
            ),
            new Slider(
              value: position.inSeconds.toDouble(),
              min: 0.0,
              max: duration.inSeconds.toDouble(),
              activeColor: Colors.red,
              inactiveColor: Colors.white,
              onChanged: (double d) {
                setState(() {
                  Duration duration = new Duration(seconds: d.toInt());
                  position = duration;
                });
              }
            ),
          ],
        ),
      ),
    );
  }

  void configureAudioPlayer() {
    audioPlayer = new AudioPlayer();
    positionSubscription = audioPlayer.onAudioPositionChanged.listen(
        (pos) => setState(() => position = pos)
    );
    stateSubscription = audioPlayer.onPlayerStateChanged.listen(
        (state) {
          if(state == AudioPlayerState.PLAYING) {
            setState(() {
              this.duration = audioPlayer.duration;
            });
          } else if(state == AudioPlayerState.STOPPED) {
            setState(() {
              this.status = PlayerState.stopped;
              forward();
            });
          }
        }, onError: (message) {
          print('error : $message');
          setState(() {
            this.status = PlayerState.stopped;
            this.duration = new Duration(seconds: 0);
            this.position = new Duration(seconds: 0);
          });
    }
    );
  }

  Future play() async {
    await audioPlayer.play(currentMusic.songUrl);
    setState(() {
      this.status = PlayerState.playing;
    });
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() {
      this.status = PlayerState.paused;
    });
  }

  void forward() {
    if(index == musics.length - 1) {
      index = 0;
    }else{
      index++;
    }
    currentMusic = musics[index];
    audioPlayer.stop();
    configureAudioPlayer();
    play();
  }

  void rewind() {
    if(index == 0) {
      index = musics.length - 1;
    } else {
      index--;
    }
    currentMusic = musics[index];
    audioPlayer.stop();
    configureAudioPlayer();
    play();
  }

  String getDuration(Duration duration) {
    return duration.toString().split('.').first;
  }

}

enum MusicAction {
  play,
  pause,
  rewind,
  forward
}

enum PlayerState {
  playing,
  paused,
  stopped
}